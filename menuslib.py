#!/home/qfwfq/Programs/environments/collatz-env/bin/python

from Tkinter import Scale, Checkbutton, HORIZONTAL, IntVar, Tk, Listbox, StringVar, DoubleVar, Entry, Label, LEFT, END, Frame, OptionMenu, BOTH


MAX_SIZE = 15
MAX_TURN = 0.3
MAX_RANGE = 10000
MAX_START = 6.4

class CollatzTools():
    def __init__(self):
        default_color = '#%02x%02x%02x' % (210,210,210)

        # Create menu
        self.master = Tk()
        self.master.configure(bg=default_color)
        self.master.tk_setPalette(background=default_color, fg=default_color)

        # Box for easier positioning within master
        self.box = Listbox(self.master)
        self.box.pack()

        # Control variables
        self.m = IntVar()
        self.s = DoubleVar()
        self.l = DoubleVar()
        self.r = DoubleVar()
        self.c = DoubleVar()

        m_slider = Scale(self.box, from_=0, to=MAX_RANGE, orient=HORIZONTAL, label="range", variable=self.m, showvalue=1)
        m_slider.pack(anchor='w')
        m_slider.set(100) # Default value is 1000

        s_slider = Scale(self.box, from_=0.5, to=MAX_SIZE, orient=HORIZONTAL, label="step size", variable=self.s, showvalue=1, resolution=0.05)
        s_slider.pack(anchor='w')
        s_slider.set(3)

        # Insert each widget with its control variable
        l_slider = Scale(self.box, from_=0, to=MAX_TURN, orient=HORIZONTAL, label="left turn", variable=self.l, showvalue=1, resolution=0.01)
        l_slider.pack(anchor='w')
        l_slider.set(0.1)

        r_slider = Scale(self.box, from_=0, to=MAX_TURN, orient=HORIZONTAL, label="right turn", variable=self.r, showvalue=1, resolution=0.01)
        r_slider.pack(anchor='w')
        r_slider.set(0.14)

        c_slider = Scale(self.box, from_=0, to=MAX_START, orient=HORIZONTAL, label="start angle", variable=self.c, showvalue=1, resolution=0.01)
        c_slider.pack(anchor='w')
        c_slider.set(3.14/2)


    def get_parameters(self):
        parameters = {
            "right turn": self.r.get(),
            "left turn": self.l.get(),
            "size": self.s.get(),
            "max": self.m.get(),
            "start": self.c.get()
        }
        self.master.update_idletasks()
        self.master.update()
        return parameters


def main():
    menu = CollatzTools()

    while 1:
        print menu.get_parameters()

if __name__ == '__main__':
    main()
