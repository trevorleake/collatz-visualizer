import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from math import cos, sin
from menuslib import CollatzTools
from screen import Screen
import numpy as np

STEP_SIZE = 4

def main():
    menu = CollatzTools()
    size = (width, height) = (1000, 1000)
    screen = Screen(size)

    while True:
        parameters = menu.get_parameters()
        paths = [collatz_path(i) for i in range(1, parameters['max'])]

        is_even = lambda x: x%2 == 0

        lines = []
        for path in paths:
            line = []
            beg = (0, 0)
            current_angle = parameters['start']
            for number in path:
                current_angle += parameters['right turn'] if is_even(number) else -1*parameters['left turn']
                x = beg[0] + (cos(current_angle) * parameters['size'])
                y = beg[1] + (sin(current_angle) * parameters['size'])
                end = (x, y)
                line.append((beg, end))
                beg = end
            lines.append(line)

        image = Image.new('RGB', size)
        draw = ImageDraw.Draw(image)
        base = (width/2, height-1-25)
        for line in lines:
            for beg, end in line:
                xb = beg[0] + base[0]
                xe = end[0] + base[0]
                yb = base[1] - beg[1]
                ye = base[1] - end[1]
                draw.line((xb, yb, xe, ye))
        screen.display(np.rot90(np.asarray(image)))


    '''
    height = max([len(path) for path in paths])*HEIGHT_SCALAR
    extremae = [extrema(path) for path in paths]
    width = max([abs(low) + high for low, high in extremae])*WIDTH_SCALAR
    size = (width, height)

    lines = []
    for path in paths:
        line = []
        x, y = (0, 0)
        for number in path[::-1]:
            if number != 1:
                beg = (x, y)
                x += WIDTH_SCALAR if is_even(number) else -1*WIDTH_SCALAR
                y += HEIGHT_SCALAR
                end = (x, y)
                line.append((beg, end))
        lines.append(line)

    base = (1, height-1)
    for line in lines:
        for beg, end in line:
            xb = beg[0] + base[0]
            xe = end[0] + base[0]
            yb = base[1] - beg[1]
            ye = base[1] - end[1]
            draw.line((xb, yb, xe, ye))

    image.show()
    '''

def extrema(path):
    is_even = lambda x: x%2 == 0

    count = 0
    min_value, max_value = (0, 0)
    for number in path:
        if is_even(number):
            count += 1
        else:
            count -= 1
        min_value = min(min_value, count)
        max_value = max(max_value, count)
    return (min_value, max_value)


def collatz_path(number):
    is_even = lambda x: x%2 == 0

    path = []
    while number != 1:
        if is_even(number):
            path.append(number)
            number = number/2
        else:
            path.append(number)
            number = (3*number)+1
    path.append(number)
    return path


if __name__ == '__main__':
    main()
